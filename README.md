# Inq Cipher Wheel Decoder/Encoder

Excessively complicated golang program to encode/decode messages based off of [Inq's Cipher Wheel](https://www.curiositybox.com/products/cipher-wheel)

## Building
From the root of the project:
```bash
go build -o inqcypher ./main.go
```
for cross-compilation set the `GOOS` and/or `GOARCH` environment variables before the `go` command


## Running
### Local file
```
go run main.go <encode/decode> [--letter <code letter>] [--values <comma-delimited values corresponding to code letter>] <message to encode/decode>
```

### Binary
```
./inqcypher <encode/decode> [--letter <code letter>] [--values <comma-delimited values corresponding to code letter>] <message to encode/decode>
```