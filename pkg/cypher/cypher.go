package cypher

import (
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

var code4specials = map[int]string{
	100: "00",
	101: "V1",
	102: "V2",
	103: "V3",
	104: "IQ", // Must be 2 characters for consistent splitting of messages after 2 characters
}
var specialChars = map[rune]string{
	'!': "!",
	'.': ".",
}

const (
	runeA = 'A'
	runeZ = 'Z'
)

// Generate an enum for each Code Level
const (
	CodeLvl1 CodeLevel = iota
	CodeLvl2
	CodeLvl3
	CodeLvl4
)

type CodeLevel int

func (cl CodeLevel) startValue() int {
	return int((cl * 26) + 1)
}

func (cl CodeLevel) endValue() int {
	return int((cl + 1) * 26)
}

func (cl CodeLevel) validate(value int) error {
	if value < cl.startValue() || value > cl.endValue() {
		return fmt.Errorf("value is outside range for code level %d. range: [%d,%d], value: %d",
			cl+1,
			cl.startValue(),
			cl.endValue(),
			value)
	}
	return nil
}

type InqCypherKey struct {
	Key             map[string]string
	startLetter     rune
	startCodes      []string
	generatorKeys   []int
	generatorLetter rune
}

// NewInqCypherKey creates a new decoding key provided the code letter and its equivalent values
// returns an instance of InqCypherKey
func NewInqCypherKey(startLetter string, startCodes string) (*InqCypherKey, error) {
	if len(startLetter) != 1 {
		return nil, fmt.Errorf("startLetter must be a single character. value: %v", startLetter)
	}
	startChar := []rune(strings.ToUpper(startLetter))[0]
	if startChar < runeA || startChar > runeZ {
		return nil, fmt.Errorf("startLetter must be between 'a' and 'z'")
	}

	ink := InqCypherKey{
		startLetter:     startChar,
		generatorLetter: startChar,
	}

	ink.startCodes = strings.Split(startCodes, ",")
	err := ink.generateNewKey()
	if err != nil {
		return nil, err
	}
	return &ink, nil
}

func (ick *InqCypherKey) setInitGeneratorKeys() (err error) {
	ick.generatorKeys = make([]int, 4)

	// invert our code4 specials map to map special strings shown in codes to integer
	invertedCode4Special := make(map[string]int)
	for key, val := range code4specials {
		invertedCode4Special[val] = key
	}

	for level := CodeLvl1; level <= CodeLvl4; level++ {
		code := ick.startCodes[level]
		if level == CodeLvl4 {
			if value, ok := invertedCode4Special[code]; ok {
				ick.generatorKeys[level] = value
				continue
			}
		}
		value, err := strconv.Atoi(code)
		if err != nil {
			return err
		}
		err = level.validate(value)
		if err != nil {
			return err
		}
		ick.generatorKeys[level] = value
	}
	return
}

// Helper for generating the key mapping by iterating through each code level
func (ick *InqCypherKey) setAndIterateLetter() {
	// Set our codes for levels 1-3 to the current letter
	for level := CodeLvl1; level <= CodeLvl3; level++ {
		ick.Key[fmt.Sprintf("%02d", ick.generatorKeys[level])] = string(ick.generatorLetter)
	}
	// Set code for level 4 as it has special values
	ick.Key[ick.getCode4()] = string(ick.generatorLetter)

	// Iterate codes and wrap around if we reach the max value for a specific Code level
	for level := CodeLvl1; level <= CodeLvl4; level++ {
		ick.generatorKeys[level]++
		if ick.generatorKeys[level] > level.endValue() {
			ick.generatorKeys[level] = level.startValue()
		}
	}
	// Iterate our letter and wrap around to 'A' if overflow past 'Z'
	ick.generatorLetter++
	if ick.generatorLetter > runeZ {
		ick.generatorLetter = runeA
	}
}

func (ick *InqCypherKey) generateNewKey() (err error) {
	err = ick.setInitGeneratorKeys()
	if err != nil {
		return
	}
	ick.Key = make(map[string]string, 26*4)

	// Set special cases first
	ick.Key[""] = " "
	ick.Key[" "] = " "
	ick.Key["."] = "."
	ick.Key["!"] = "!"

	for i := 0; i < 26; i++ {
		ick.setAndIterateLetter()
	}
	return
}

func (ick *InqCypherKey) getCode4() string {
	code4 := ick.generatorKeys[CodeLvl4]
	if code4 >= 100 {
		return code4specials[code4]
	}
	return fmt.Sprintf("%02d", code4)
}

// invertKey reverses the order of the Key field mapping an array of the
// strings of codes that represnt a single letter
func (ick *InqCypherKey) invertKey() map[string][]string {
	newKey := make(map[string][]string, 26)
	for key, val := range ick.Key {
		// Create new []string for val if not currently in inverted mapping
		if _, ok := newKey[val]; !ok {
			newKey[val] = []string{key}
			continue
		}
		newKey[val] = append(newKey[val], key)
	}
	newKey[" "] = []string{" "}
	return newKey
}

// DecodeMessage takes in an encoded message with space-delimited words
// Returns an all-lower case version of the message
func (ick *InqCypherKey) DecodeMessage(message string) string {
	splitMessage := splitCodedMessage(message)
	finalString := ""
	for _, val := range splitMessage {
		finalString += ick.Key[val]
	}
	return strings.ToLower(finalString)
}

// EncodeMessage takes in a sentence or phrase with space delimited words and converts it into
// a single encoded string.
// A randomizer selects which level of the encoder to take a code to represent a letter
func (ick *InqCypherKey) EncodeMessage(message string) string {
	finalString := ""
	message = strings.ToUpper(message)
	rand.Seed(time.Now().UnixNano())
	swappedKey := ick.invertKey()
	var random int
	for _, val := range message {
		arr, ok := swappedKey[string(val)]
		if !ok {
			continue
		}
		if len(arr) < 1 {
			continue
		}
		random = rand.Intn(len(arr))
		finalString += swappedKey[string(val)][random]
	}
	return finalString
}

// splitCodedMessage expects a space-delimited string of "cyphered integer" words.
// The coded message is split by space and iterates through each "word" splitting each word
// by 2 characters to collect the 0-padded 2 digit integers.
func splitCodedMessage(coded string) []string {
	splitMessage := strings.Split(coded, " ")
	finalMessage := make([]string, 0)
	for _, word := range splitMessage {
		singleInt := ""
		for i, char := range word {
			// Check if the current character is one of the "special" punctuation chars
			if _, ok := specialChars[char]; ok {
				finalMessage = append(finalMessage, string(char))
				continue
			}

			// Odd indices occur every 2 characters. combine the last 2 characters into single string.
			// Bug: If you put punctuation in the middle of a word this will ruin all of this logic
			if i%2 == 1 {
				finalMessage = append(finalMessage, singleInt+string(char))
				continue
			}
			singleInt = string(char)

		}
		// Append empty strings to signify the end of a word.
		finalMessage = append(finalMessage, "")
	}
	return finalMessage
}

// func main() {
// 	// message := "552283908376832926 30 024856 V2815804 6114 12630207 620445V2 699651 3041 525650140352V1 08V2 IQ871781 38207750321167!"
// 	// message := "122283906907690769 08716996V2673752V114 34V38576 62878019 6913729162 73968514035240776264043981 99427771109446!"
// 	message := "127969904829697683 91 852696 V2461573 4014 55V35007 V2046641 839603 0819 87565097518718 30V2 21871767 59423085539481!"
// 	inq, _ := NewInqCypherKey('V', "21,43,64,IQ")
// 	output := inq.DecodeMessage(message)
// 	fmt.Println(output)
// 	sendMessage := "This is fascinating to decode and encode things at my own will"
// 	newMessage := inq.EncodeMessage(sendMessage)
// 	fmt.Println(newMessage)
// }
