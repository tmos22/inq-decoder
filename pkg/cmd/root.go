package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	cypher "gitlab.com/tmos22/inq-decoder/pkg/cypher"
)

var rootCmd = &cobra.Command{
	Use:              "inqcypher",
	Short:            "Inq Cypher decodes and encodes messages",
	Args:             cobra.MinimumNArgs(1),
	PersistentPreRun: persistentPre,
}

var encodeCmd = &cobra.Command{
	Use:   "encode",
	Short: "Encode a message with the provided key",
	Args:  cobra.MinimumNArgs(1),
	Run:   encodeCypher,
}

var decodeCmd = &cobra.Command{
	Use:   "decode",
	Short: "Decodes a cyphered message with the provided key",
	Args:  cobra.MinimumNArgs(1),
	Run:   decodeCypher,
}

var ink *cypher.InqCypherKey

func init() {
	rootCmd.PersistentFlags().String("letter", "A",
		"Letter used to identify decoding cypher")
	rootCmd.PersistentFlags().String("values", "01,27,53,79",
		"the four code values corresponding to the provided letter")
	if err := viper.BindPFlags(rootCmd.PersistentFlags()); err != nil {
		panic(fmt.Sprintf("Failed to bind pflags: %v", err))
	}
	rootCmd.AddCommand(decodeCmd)
	rootCmd.AddCommand(encodeCmd)
}

func persistentPre(cmd *cobra.Command, args []string) {
	var err error
	ink, err = cypher.NewInqCypherKey(viper.GetString("letter"), viper.GetString("values"))
	if err != nil {
		fmt.Printf("Error creating new Cypher Key: %v", err)
		os.Exit(1)
	}
}

func decodeCypher(cmd *cobra.Command, args []string) {
	message := strings.Join(args, " ")
	decoded := ink.DecodeMessage(message)
	fmt.Println(decoded)
}

func encodeCypher(cmd *cobra.Command, args []string) {
	message := strings.Join(args, " ")
	encoded := ink.EncodeMessage(message)
	fmt.Println(encoded)
}

func Execute() error {
	return rootCmd.Execute()
}
